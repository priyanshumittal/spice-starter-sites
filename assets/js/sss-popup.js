(function($){
    
$(document).ready(function() {
    // Show all content by default
    $('.tab-item').addClass('show');

    $('.tab-button').on('click', function() {
        var tab = $(this).data('tab');
        
        $('.tab-button').removeClass('active');
        $(this).addClass('active');
        
        if (tab === 'all') {
            $('.tab-item').addClass('show');
        } else {
            $('.tab-item').each(function() {
                if ($(this).data('tab') === tab) {
                    $(this).addClass('show');
                } else {
                    $(this).removeClass('show');
                }
            });
        }
    });
});


})(jQuery);