=== Spice Starter Sites ===

Contributors: 		spicethemes
Requires at least: 	5.3
Requires PHP: 		5.2
Tested up to: 		6.7.1
Stable tag: 		1.3.2.2
License: 		GPLv2 or later
License URI: 		https://www.gnu.org/licenses/gpl-2.0.html

This plugin allows you to import all the demos offered by our Newscrunch and OliveWP themes.

== Description ==

The plugin allows you to create professional designed pixel perfect websites in minutes. Import the stater sites to create the beautiful websites.

== Installation ==

1. Upload `spice-starter-sites` to the `/wp-content/plugins/` directory;
2. Activate the plugin through the 'Plugins > Installed Plugins' menu in WordPress dashboard;
3. Done!

== Changelog ==

@Version 1.3.2.2
* Added 2 block starter sites for NewsBlogger Child Theme and Newscrunch Plus.

@Version 1.3.2.1
* Fixed demo import warning issue in the Appointment theme.

@Version 1.3.2
* Updated freemius directory.

@Version 1.3.1
* Added starter sites for Appointment and Appointment Pro themes.

@Version 1.3
* Fixed the issues mentioned by the reviewer.

@Version 1.2.5
* Added 2 new starter sites for the Newscrunch Plus.

@Version 1.2.4
* Added new starter sites for the NewsBlogger Child Theme and Newscrunch Plus.

@Version 1.2.3
* Fixed reading setting issue for the Newscrunch theme demo import.

@Version 1.2.2
* Added 2 new starter sites for the Newscrunch Plus.

@Version 1.2.1
* Added 3 new starter sites for the Newscrunch Plus.

@Version 1.2
* Added new starter sites for the Newscrunch Plus.

@Version 1.1
* Updated freemius directory.

@Version 1.0
* Added import feature for Newscrunch theme.

@Version 0.5
* Added new starter sites.

@Version 0.4
* Added new starter sites.

@Version 0.3
* Added new starter sites.

@Version 0.2
* Changed the demo preview URL's.

@Version 0.1
* Initial Release.

== External resources ==

Demo Content Import:
Source: https://spicethemes.com/ and https://olivewp.org
Purpose: Fetches demo content like sample data, demo images for Newscrunch and OliveWP themes.
Terms of Service: https://spicethemes.com/terms-and-conditions/
Privacy Policy: https://spicethemes.com/privacy-policy/